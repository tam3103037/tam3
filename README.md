
| Title | Student management updates faculty information |
| ------ | ------ |
|**Value statement**|As a student manager, I want to update the faculties information to show the faculties status|
|**Acceptace Criteria**       | ***Scenario 1: Show faculties***       |
|        |  Given that I have faculties      |
||When I click on show button|
||Then I can see the information of the faculties|
||***Scenario 2: Add a majors***|
||Given that I have a number of students and lecturers|
||Then I click on Add button|
||Then I will have specialized information about the department stored in the database|
||***Scenario 3: Edit faculty information***|
||Given that I have information about students in a department and I have information and number of students changing majors|
||When I click on Update button|
||Then I will update the department's information and the department's information will be saved in the database|
|**Definition of done**|Unit Tests Passed|
||Acceptace Criteria Met|
||Code Reviewed|
||Functional Tests Passed|
||Non-Functional Requirement Met|
||Product Owner Accepts User Story|
|**Owner**|Nguyen Ba Tam|
|**Interation**|Unscheduled|
|**Estimate**|10 points|
